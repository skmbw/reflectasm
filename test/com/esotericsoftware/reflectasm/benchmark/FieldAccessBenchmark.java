/**
 * Copyright (c) 2008, Nathan Sweet
 *  All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 *
 *  1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 *  2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 *  3. Neither the name of Esoteric Software nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

package com.esotericsoftware.reflectasm.benchmark;

import com.esotericsoftware.reflectasm.FieldAccess;

import java.lang.reflect.Field;

/**
 * 加上初始化的时间，jdk的性能是比这个要好的。jdk的初始化，method、field、constructor都比这个快。5800H上，4.5对8.8毫秒
 * 在Ubuntu上，jdk11,测试的结果，长时间运行后，reflectasm会好10%左右，12ms左右。一开始（随机访问）反而是jdk的性能好一些，14ms左右。
 * FieldAccess: 12.236927 ms
 * FieldAccess: 11.848096 ms
 * FieldAccess: 12.161191 ms
 * FieldAccess: 11.931346 ms
 * FieldAccess: 12.228815 ms
 * FieldAccess: 14.457802 ms
 * FieldAccess: 15.024563 ms
 * FieldAccess: 11.552628 ms
 * FieldAccess: 11.519385 ms
 * FieldAccess: 11.475838 ms
 * FieldAccess: 11.490047 ms
 * FieldAccess: 11.477387 ms
 * FieldAccess: 11.492744 ms
 * Reflection: 13.347545 ms
 * Reflection: 13.252359 ms
 * Reflection: 13.330305 ms
 * Reflection: 14.227007 ms
 * Reflection: 13.62705 ms
 * Reflection: 13.897132 ms
 * Reflection: 13.863982 ms
 * Reflection: 14.002131 ms
 * Reflection: 13.674929 ms
 * Reflection: 13.822813 ms
 * Reflection: 13.842591 ms
 * Reflection: 13.509913 ms
 * Reflection: 13.358722 ms
 * Reflection: 13.52937 ms
 * Reflection: 13.462717 ms
 */
public class FieldAccessBenchmark extends Benchmark {
	public FieldAccessBenchmark () throws Exception {
		int count = 1000000;
		Object[] dontCompileMeAway = new Object[count];

		long d = System.currentTimeMillis();
		FieldAccess access = FieldAccess.get(SomeClass.class);
		int index = access.getIndex("name");
		System.out.println(System.currentTimeMillis() - d); // 约40ms

		d = System.currentTimeMillis();
		Field field = SomeClass.class.getField("name");
		System.out.println(System.currentTimeMillis() - d); // 约0ms

		SomeClass someObject = new SomeClass();
		for (int i = 0; i < 100; i++) {
			for (int ii = 0; ii < count; ii++) {
				access.set(someObject, index, "first");
				dontCompileMeAway[ii] = access.get(someObject, index);
			}
			for (int ii = 0; ii < count; ii++) {
				field.set(someObject, "first");
				dontCompileMeAway[ii] = field.get(someObject);
			}
		}
		warmup = false;

		for (int i = 0; i < 100; i++) {
			start();
			for (int ii = 0; ii < count; ii++) {
				access.set(someObject, index, "first");
				dontCompileMeAway[ii] = access.get(someObject, index);
			}
			end("FieldAccess");
		}
		for (int i = 0; i < 100; i++) {
			start();
			for (int ii = 0; ii < count; ii++) {
				field.set(someObject, "first");
				dontCompileMeAway[ii] = field.get(someObject);
			}
			end("Reflection");
		}

		chart("Field Set/Get");
	}

	static public class SomeClass {
		public String name;
	}

	public static void main (String[] args) throws Exception {
		new FieldAccessBenchmark();
	}
}
